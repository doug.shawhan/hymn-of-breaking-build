# Hymn of Breaking Build

**THE** RFCs are truthful  
 (Let all who code take heed!)  
Whether building something useful  
 or yet another thneed.  
So when your nested fails befunge  
 And stacks are overstuffed,  
Blame not the feckless user's whinge,  
 'Cause fool, you wrote this cruft!  
  *Don't blame the tools -- it's cruft!*

There, in your daily stand-up  
 'Spite the shuffled kanban's pleas  
Scrum masters ear-holes plugged up  
 With c-suite fantasies.  
With no goal chiseled in their skull  
 beyond "pivot toward the cash",  
the kristal-spilling gravatic pull  
 of "angel round, or crash":  
  *O Nerd! Retrain! -- THE CRASH!*  

Sphinx obtusely renders  
 Spectral pointers to the code  
Hearalding silent failures  
  unit tests exposed.  
Your merge requests now fester  
 As conflicts shift the blame  
You see the new maintainer:  
 The noob you once had flamed!  
  *The flamer -- now the flamed!*  

You feast upon stackoverflow  
 And Knuth's eternal trust  
With time compressed, and mind not blown:  
 "I'll finally learn Rust!"  
Though Ronacher's advisement  
 And counsel is most wise  
A bootcamp advertisement  
 Has hooked the boss's eyes!  
  *Will no one shield his eyes?!*  

O willing slaves of adderall  
 (Who poorly serialize)  
Recursively face-planting all  
 Your too-truncated lives.  
Now wise, and with the ackles  
 To spin one perfect fix  
Still chafed by gilded shackles  
 Like shrike's prey upon pricks.  
  *Still -- kick against the pricks!*  

O sweet, well crafted GPL  
 On which it all abides  
Be our clothespin for the smell  
 Of management's low tide;  
That we - with service tokens  
 May strain to keep it real  
Though in pain and mostly broken,  
 *We are tracking what is broken   
  We'll fix it next release  
  "We're still beta! Next release!*


# Career<sup>6 <em>(archaic)</em></sup> 

> Sleep on your back and ash in your shoes  
> And always use the old sense of the words
> - Silver Jews "Advice to the Graduate"

One can very seriously pursue a career and run headlong into a wall. Another can stumble upon a thing and, for whatever reason, end up get paid pretty well. The secret is to never make a traceable mistake, and know just exactly when to hide, quit, or run. Aw, come on! Hugs!

# Imagine a Boot

People who pretend to understand things for a living on the internet enjoy saying "retrain", or more specifically, "learn to code" when bloviating about the Slavering Economic Displacement that is quietly sniffing at their own tendons. These assertions have recently been almost universally accompanied by ads for Python training. These "Boot Camps", which teach you as much about being a programmer as real boot camps teach you about wilderness shoe repair, are business models that have bloomed like internet like hot, flaky staph since the late 1990s.

This is not Python's fault. Nor was it the fault of Perl, PHP, Ruby, Javascript or any of the worthy scripting languages that have been flogged and ditched to varying degrees over the last two decades.

Python is no different, except I think it may be.

There are [cute animated graphs](https://statisticsanddata.org/data/the-most-popular-programming-languages-1965-2021/) that demonstrate the popularity (I know ...) of programming languages over time. Python's growth has been of a slow and steady nature. It is now slightly more popular than Javascript, which has an interpreter built into every browser you have ever used (unless you are really, really old). A staggering advantage.

If you watched the animated graphs linked above, you probably noticed the rise, and eventual equilibrium, of the C language in the aforementioned cute graph. Do a little reading, and you will find that C was once considered "high-level" (which means, in a nutshell, "written for humans to read and maintain"), "bloated" (which means it has libraries of code you can reuse) and "slow" (which means whatever you can get away with). C is now none of those things, because of continual improvement, improving hardware and know-it-alls retiring. People really like C, and it continues to improve, slowly and surely, and is not going to die any time soon, [Rust](https://www.rust-lang.org/) notwithstanding.

# You Sound like Someone Pretending to Understand Things for a Living on the Internet

I'm totally going to try and help you learn some Python, but I'm going to focus more on why one might want to take up programming as an avocation, and how to think of code as something you will pass on to others to learn from and improve, and how others have built some wonderful tools focused on just that end.

I expect to recieve and ignore criticism that [Zed Shaw](https://www.amazon.com/Learn-Python-Hard-Way-Introduction/dp/0134692888), [Allen Downey](https://openbookproject.net/thinkcs/python/english3e/) and a hundred others did it better. I won't argue that. This collection of executable self-indulgence is not so much a "how to code" guide as a vehicle for thinking about improving whatever one can by encouraging incrementalism, reversability and genial forking.

As usual, just click on the various chapters contained in their own directories. If my code looks ratty, and you have a better idea, I'll surely enjoy the pull requests. If you don't know what a pull request is, you are in for a treat.
